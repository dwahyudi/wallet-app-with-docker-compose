package cfg

import (
	"log"

	"github.com/spf13/viper"
)

// WebConfig stores values for web server configurations.
type WebConfig struct {
	ServerPort string
}

/*
WebSetup returns necessary Config struct containing config and secret values for server.
*/
func WebSetup() WebConfig {
	// Env Keys Name
	serverPortKey := "WALLET_SERVER_PORT"

	// Sets default values
	viper.SetDefault(serverPortKey, ":7777")

	serverPort, ok := viper.Get(serverPortKey).(string)
	if !ok {
		log.Panicf("Invalid type assertion for server port")
	}

	return WebConfig{
		ServerPort: serverPort,
	}
}
