package webservice

type WalletCreateReqParam struct {
	StartingBalance float64 `json:"starting_balance" binding:"required,gte=0"`
	Ktp             string  `json:"ktp" binding:"required"`
	Name            string  `json:"name" binding:"required"`
	Email           string  `json:"email" binding:"required"`
	PhoneNumber     string  `json:"phone_number" binding:"required"`
}

type WalletTopupReqParam struct {
	SerialNum string  `json:"serial_number" binding:"required"`
	Amount    float64 `json:"amount" binding:"required"`
}

type WalletPayReqParam struct {
	SerialNum string  `json:"serial_number" binding:"required"`
	Amount    float64 `json:"amount" binding:"required"`
}

type WalletCheckBalanceReqParam struct {
	SerialNum string `json:"serial_number" binding:"required"`
}
