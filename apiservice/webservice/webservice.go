package webservice

import (
	"log"
	"mywallet/apiservice/cfg"

	"github.com/gin-gonic/gin"
)

// LaunchWeb starts server.
func LaunchWeb() {
	log.Println("Starting mywallet API Service")
	router := gin.Default()

	router.GET("/ping", ping)

	router.POST("/wallet/topup", topup)
	router.POST("/wallet/create", create)
	router.POST("/wallet/pay", pay)
	router.GET("/wallet/checkbalance", checkBalance)

	port := cfg.WebSetup().ServerPort

	router.Run(port)
}
