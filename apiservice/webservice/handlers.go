package webservice

import (
	"context"
	"fmt"
	"log"
	v1 "mywallet/api/v1"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
)

func ping(c *gin.Context) {
	c.String(http.StatusOK, "pong")
}

func checkBalance(c *gin.Context) {
	var wcbrp WalletCheckBalanceReqParam

	bindErr := c.BindJSON(&wcbrp)
	if bindErr != nil {
		c.JSON(http.StatusBadRequest, fmt.Sprint(bindErr))
	}

	var conn *grpc.ClientConn
	conn, err := grpc.Dial(fmt.Sprintf("wallet:%v", walletGrpcPort()), grpc.WithInsecure())
	if err != nil {
		log.Panicf("checkbalance - did not connect: %s", err)
	}
	defer conn.Close()

	cl := v1.NewWalletsClient(conn)

	resp, err := cl.CheckBalance(context.Background(), &v1.WalletCheckBalanceParam{SerialNum: wcbrp.SerialNum})
	if err != nil {
		log.Panicf("checkbalance - Error when calling topup: %s", err)
	}
	c.JSON(http.StatusOK, resp.Balance)
}

func create(c *gin.Context) {
	var wcrp WalletCreateReqParam

	bindErr := c.BindJSON(&wcrp)
	if bindErr != nil {
		c.JSON(http.StatusBadRequest, fmt.Sprint(bindErr))
	}

	// Create Identity
	var idconn *grpc.ClientConn
	idconn, err := grpc.Dial(fmt.Sprintf("identity:%v", identityGrpcPort()), grpc.WithInsecure())
	if err != nil {
		log.Panicf("create - did not connect: %s", err)
	}
	defer idconn.Close()

	idClient := v1.NewIdentitiesClient(idconn)

	idResp, err := idClient.Create(context.Background(), &v1.CreateIdentityParam{
		Ktp:         wcrp.Ktp,
		Name:        wcrp.Name,
		Email:       wcrp.Email,
		PhoneNumber: wcrp.PhoneNumber,
	})
	if err != nil {
		log.Panicf("create - Error when calling create: %s", err)
	}

	// Create Wallet
	var wconn *grpc.ClientConn
	wconn, err = grpc.Dial(fmt.Sprintf("wallet:%v", walletGrpcPort()), grpc.WithInsecure())
	if err != nil {
		log.Panicf("create - did not connect: %s", err)
	}
	defer wconn.Close()

	wClient := v1.NewWalletsClient(wconn)

	resp, err := wClient.Create(context.Background(), &v1.WalletCreateParam{IdentityID: idResp.Id, StartingBalance: wcrp.StartingBalance})
	if err != nil {
		log.Panicf("create - Error when calling topup: %s", err)
	}
	c.JSON(http.StatusCreated, fmt.Sprintf("Successful wallet creation, serial number:%v", resp.SerialNum))
}

func topup(c *gin.Context) {
	var wtrp WalletTopupReqParam

	bindErr := c.BindJSON(&wtrp)
	if bindErr != nil {
		c.JSON(http.StatusBadRequest, fmt.Sprint(bindErr))
	}

	// Check identity
	isActive := checkIdentity(wtrp.SerialNum)

	// If identity is invalid, halt topup
	if !isActive {
		c.JSON(http.StatusUnprocessableEntity, "user identity is not active")
		return
	}

	var conn *grpc.ClientConn
	conn, err := grpc.Dial(fmt.Sprintf("wallet:%v", walletGrpcPort()), grpc.WithInsecure())
	if err != nil {
		log.Panicf("topup - did not connect: %s", err)
	}
	defer conn.Close()
	cl := v1.NewWalletsClient(conn)

	_, err = cl.Topup(context.Background(), &v1.WalletTopupParam{SerialNum: wtrp.SerialNum, Amount: wtrp.Amount})
	if err != nil {
		log.Panicf("topup - Error when calling topup: %s", err)
	}
	c.JSON(http.StatusCreated, "Successful topup")
}

func pay(c *gin.Context) {
	var wprp WalletPayReqParam

	bindErr := c.BindJSON(&wprp)
	if bindErr != nil {
		c.JSON(http.StatusBadRequest, fmt.Sprint(bindErr))
	}

	// Check identity
	isActive := checkIdentity(wprp.SerialNum)

	// If identity is invalid, halt pay
	if !isActive {
		c.JSON(http.StatusUnprocessableEntity, "user identity is not active")
		return
	}

	var conn *grpc.ClientConn
	conn, err := grpc.Dial(fmt.Sprintf("wallet:%v", walletGrpcPort()), grpc.WithInsecure())
	if err != nil {
		log.Panicf("pay - did not connect: %s", err)
	}
	defer conn.Close()

	cl := v1.NewWalletsClient(conn)

	_, err = cl.Pay(context.Background(), &v1.WalletPayParam{SerialNum: wprp.SerialNum, Amount: wprp.Amount})
	if err != nil {
		if strings.Contains(err.Error(), "insufficient balance") {
			c.JSON(http.StatusUnprocessableEntity, "insufficient balance")
		} else {
			log.Panicf("pay - Error when calling topup: %s", err)
		}
		return
	}
	c.JSON(http.StatusCreated, "Successful payment")
}

func walletGrpcPort() string {
	viper.SetConfigFile(".env")
	if err := viper.ReadInConfig(); err != nil {
		fmt.Printf("wallet grpc port - Error reading config file, %s", err)
	}
	port := viper.Get("WALLET_GRPC_PORT").(string)

	return port
}

func identityGrpcPort() string {
	viper.SetConfigFile(".env")
	if err := viper.ReadInConfig(); err != nil {
		fmt.Printf("identity grpc port - Error reading config file, %s", err)
	}
	port := viper.Get("IDENTITY_GRPC_PORT").(string)

	return port
}

func checkIdentity(serialNum string) bool {
	// Obtain identity ID from serial number
	var conn *grpc.ClientConn
	conn, err := grpc.Dial(fmt.Sprintf("wallet:%v", walletGrpcPort()), grpc.WithInsecure())
	if err != nil {
		log.Panicf("check id - did not connect: %s", err)
	}
	defer conn.Close()

	cl := v1.NewWalletsClient(conn)

	resp, err := cl.GetIdentityIDBySerialNum(context.Background(), &v1.WalletSerialNum{SerialNum: serialNum})
	if err != nil {
		log.Panicf("check id - Error when check id to wallet grpc: %s", err)
	}

	// Check identity
	var idconn *grpc.ClientConn
	idconn, err = grpc.Dial(fmt.Sprintf("identity:%v", identityGrpcPort()), grpc.WithInsecure())
	if err != nil {
		log.Panicf("check id - did not connect: %s", err)
	}
	defer idconn.Close()

	idClient := v1.NewIdentitiesClient(idconn)

	idResp, err := idClient.CheckActive(context.Background(), &v1.IdentityID{Id: resp.IdentityID})
	if err != nil {
		log.Panicf("check - Error when check id to identity grpc: %s", err)
	}

	return idResp.IsActive
}
