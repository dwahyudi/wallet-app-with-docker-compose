module mywallet

go 1.15

require (
	github.com/dchest/uniuri v0.0.0-20200228104902-7aecb25e1fe5
	github.com/gin-gonic/gin v1.6.3
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang-migrate/migrate v3.5.4+incompatible
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/golang/protobuf v1.4.3
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/lib/pq v1.8.0
	github.com/spf13/viper v1.7.1
	google.golang.org/grpc v1.33.1
	google.golang.org/protobuf v1.25.0
)
