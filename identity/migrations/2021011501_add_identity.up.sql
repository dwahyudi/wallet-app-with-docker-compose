CREATE TABLE identity (
	id serial PRIMARY KEY,
	ktp VARCHAR ( 50 ) UNIQUE NOT NULL,
	name VARCHAR ( 300 ) NOT NULL,
    email VARCHAR ( 200 ) NOT NULL UNIQUE NOT NULL,
	phone_number VARCHAR ( 40 ) NOT NULL UNIQUE NOT NULL,
	status INTEGER NOT NULL,
	created_at TIMESTAMP NOT NULL DEFAULT current_timestamp
);