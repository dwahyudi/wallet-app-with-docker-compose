package mystore

func CreateIdentity(cisp CreateIdentityStoreParam) (IdentityID, error) {
	db := DBConn()
	defer db.Close()

	query := "INSERT INTO identity (ktp, name, email, phone_number, status) VALUES(?, ?, ?, ?, ?);"
	stmt, stmtErr := db.Prepare(query)
	defer stmt.Close()
	if stmtErr != nil {
		return IdentityID{}, stmtErr
	}

	resp, queryErr := stmt.Exec(cisp.Ktp, cisp.Name, cisp.Email, cisp.PhoneNumber, cisp.Status)
	if queryErr != nil {
		return IdentityID{}, stmtErr
	}

	id, getLastInsertIdErr := resp.LastInsertId()
	if getLastInsertIdErr != nil {
		return IdentityID{}, getLastInsertIdErr
	}

	return IdentityID{ID: id}, nil
}

func CheckActive(isn IdentityID) (IdentityStatus, error) {
	db := DBConn()
	defer db.Close()

	var status int32
	query := "SELECT status FROM identity WHERE id = ?"
	row := db.QueryRow(query, isn.ID)
	row.Scan(&status)

	return IdentityStatus{ID: isn.ID, Status: status}, nil
}
