package mystore

import (
	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database/mysql"
	_ "github.com/golang-migrate/migrate/source/file"
)

func Up() {
	db := DBConn()
	defer db.Close()

	driver, err := mysql.WithInstance(db, &mysql.Config{})
	if err != nil {
		log.Panicf("Cannot setup driver, %s", err)
	}

	m, err := migrate.NewWithDatabaseInstance(
		"file://identity/migrations",
		"mysql",
		driver,
	)

	if err != nil {
		log.Panicf("Cannot setup migration, %s", err)
	}

	m.Up()
}
