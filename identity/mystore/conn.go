package mystore

import (
	"database/sql"
	"fmt"
	"log"
	"strconv"

	"github.com/spf13/viper"
)

func DBConn() *sql.DB {
	viper.SetConfigFile(".env")
	if err := viper.ReadInConfig(); err != nil {
		fmt.Printf("Error reading config file, %s", err)
	}

	myHostKey := "IDENTITY_MYSQL_DB_HOST"
	myHostPort := "IDENTITY_MYSQL_DB_PORT"
	myDbNameKey := "IDENTITY_MYSQL_DB_NAME"
	myDbUserKey := "IDENTITY_MYSQL_DB_USER"
	myDbPassKey := "IDENTITY_MYSQL_DB_PASS"

	viper.SetDefault(myHostKey, "localhost")
	viper.SetDefault(myHostPort, "5433")
	viper.SetDefault(myDbNameKey, "identity")
	viper.SetDefault(myDbUserKey, "root")
	viper.SetDefault(myDbPassKey, "password")

	myHost, ok := viper.Get(myHostKey).(string)
	if !ok {
		log.Panicf("Invalid type assertion for mysql host")
	}

	myPort, ok := viper.Get(myHostPort).(string)
	if !ok {
		log.Panicf("Invalid type assertion for mysql port")
	}

	myDbName, ok := viper.Get(myDbNameKey).(string)
	if !ok {
		log.Panicf("Invalid type assertion for mysql db name")
	}

	myUser, ok := viper.Get(myDbUserKey).(string)
	if !ok {
		log.Panicf("Invalid type assertion for mysql user")
	}

	myPass, ok := viper.Get(myDbPassKey).(string)
	if !ok {
		log.Panicf("Invalid type assertion for mysql password")
	}

	port, err := strconv.Atoi(myPort)
	if err != nil {
		log.Panicf("cannot obtain port, %s", err)
	}

	// example: root:123456@tcp(127.0.0.1:3306)/dbname
	connString := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v", myUser, myPass, myHost, port, myDbName)
	db, err := sql.Open("mysql", connString)
	if err != nil {
		log.Panicf("Cannot connect to mysql, %s", err)
	}

	return db
}
