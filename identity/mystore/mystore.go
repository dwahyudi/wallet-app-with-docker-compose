package mystore

type CreateIdentityStoreParam struct {
	Ktp         string
	Name        string
	Email       string
	PhoneNumber string
	Status      int
}

type IdentityID struct {
	ID int64
}

type IdentityStatus struct {
	ID     int64
	Status int32
}
