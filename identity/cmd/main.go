package main

import (
	"context"
	"fmt"
	"log"
	v1 "mywallet/api/v1"
	"mywallet/identity/mystore"
	"net"

	_ "github.com/lib/pq"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
)

func main() {
	mystore.Up()

	viper.SetConfigFile(".env")
	if err := viper.ReadInConfig(); err != nil {
		fmt.Printf("Error reading config file, %s", err)
	}
	port := viper.Get("IDENTITY_GRPC_PORT").(string)

	log.Println("Starting mywallet Identity GRPC Service")
	log.Println("Listening on", port)

	lis, err := net.Listen("tcp", fmt.Sprintf(":%v", port))
	if err != nil {
		log.Panicf("failed to listen: %v", err)
	}

	idsv := IdentityServer{}
	grpcServer := grpc.NewServer()

	v1.RegisterIdentitiesServer(grpcServer, &idsv)

	if err := grpcServer.Serve(lis); err != nil {
		log.Panicf("failed to serve: %s", err)
	}
}

type IdentityServer struct {
	v1.UnimplementedIdentitiesServer
}

func (idsv *IdentityServer) Create(ctx context.Context, b *v1.CreateIdentityParam) (*v1.IdentityID, error) {
	cisp := mystore.CreateIdentityStoreParam{
		Ktp:         b.GetKtp(),
		Name:        b.GetName(),
		Email:       b.GetEmail(),
		Status:      int(v1.Status_ACTIVE),
		PhoneNumber: b.GetPhoneNumber(),
	}
	resp, err := mystore.CreateIdentity(cisp)
	if err != nil {
		return &v1.IdentityID{}, err
	}

	return &v1.IdentityID{Id: resp.ID}, nil
}

func (idsv *IdentityServer) CheckActive(ctx context.Context, b *v1.IdentityID) (*v1.IdentityStatus, error) {
	isn := mystore.IdentityID{ID: b.GetId()}
	resp, err := mystore.CheckActive(isn)
	if err != nil {
		return &v1.IdentityStatus{}, err
	}

	isActive := resp.Status == v1.Status_value["ACTIVE"]

	return &v1.IdentityStatus{Id: resp.ID, IsActive: isActive}, nil
}
