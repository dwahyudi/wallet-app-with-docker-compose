package pgstore

type CreateWalletStoreParam struct {
	IdentityID int64
	Balance    float64
}

type CreateWalletStoreResponse struct {
	SerialNum string
	Balance   float64
}

type TopupWalletStoreParam struct {
	SerialNum string
	Amount    float64
}

type TopupWalletStoreResponse struct {
	SerialNum string
	Balance   float64
}

type PayFromWalletStoreParam struct {
	SerialNum string
	Amount    float64
}

type PayFromWalletStoreResponse struct {
	SerialNum string
	Balance   float64
}

type CheckBalanceWalletStoreParam struct {
	SerialNum string
}

type CheckBalanceWalletStoreResponse struct {
	SerialNum string
	Balance   float64
}

type GetIdentityIDBySerialNumStoreParam struct {
	SerialNum string
}

type IdentityIDWalletStoreResponse struct {
	IdentityID int64
}
