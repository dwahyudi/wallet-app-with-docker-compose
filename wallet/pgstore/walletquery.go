package pgstore

import (
	"context"
	"errors"

	"github.com/dchest/uniuri"
)

func CreateWallet(cwsp CreateWalletStoreParam) (CreateWalletStoreResponse, error) {
	db := DBConn()
	defer db.Close()

	serialNum := uniuri.New()

	query := "INSERT INTO wallet (serialnum, balance, identity_id) VALUES($1, $2, $3);"
	stmt, stmtErr := db.Prepare(query)
	defer stmt.Close()
	if stmtErr != nil {
		return CreateWalletStoreResponse{}, stmtErr
	}

	_, queryErr := stmt.Exec(serialNum, cwsp.Balance, cwsp.IdentityID)
	if queryErr != nil {
		return CreateWalletStoreResponse{}, stmtErr
	}

	return CreateWalletStoreResponse{SerialNum: serialNum, Balance: cwsp.Balance}, nil
}

func TopupWallet(twsp TopupWalletStoreParam) (TopupWalletStoreResponse, error) {
	db := DBConn()
	defer db.Close()

	// Lock and Enclose in transaction
	ctx := context.Background()
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		return TopupWalletStoreResponse{}, err
	}

	// Lock
	var currBalance float64
	query := "SELECT balance FROM wallet WHERE serialnum = $1 LIMIT 1"
	row := db.QueryRow(query, twsp.SerialNum)
	row.Scan(&currBalance)

	// Calculate
	updatedBalance := currBalance + twsp.Amount

	// Prepared statement
	updateQuery := "UPDATE wallet SET balance = $1 WHERE serialnum = $2"
	stmt, stmtErr := db.PrepareContext(ctx, updateQuery)
	if err != nil {
		tx.Rollback()
		return TopupWalletStoreResponse{}, stmtErr
	}

	// Execute update
	_, queryErr := stmt.ExecContext(ctx, updatedBalance, twsp.SerialNum)
	if queryErr != nil {
		tx.Rollback()
		return TopupWalletStoreResponse{}, stmtErr
	}

	err = tx.Commit()
	if err != nil {
		return TopupWalletStoreResponse{}, stmtErr
	}

	return TopupWalletStoreResponse{SerialNum: twsp.SerialNum, Balance: updatedBalance}, nil
}

func CheckBalanceWallet(cbwsp CheckBalanceWalletStoreParam) (CheckBalanceWalletStoreResponse, error) {
	db := DBConn()
	defer db.Close()

	var currBalance float64
	query := "SELECT balance FROM wallet WHERE serialnum = $1 LIMIT 1"
	row := db.QueryRow(query, cbwsp.SerialNum)
	row.Scan(&currBalance)

	return CheckBalanceWalletStoreResponse{SerialNum: cbwsp.SerialNum, Balance: currBalance}, nil
}

func PayFromWallet(pfwsp PayFromWalletStoreParam) (PayFromWalletStoreResponse, error) {
	db := DBConn()
	defer db.Close()

	// Lock and Enclose in transaction
	ctx := context.Background()
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		return PayFromWalletStoreResponse{}, err
	}

	// Lock
	var currBalance float64
	query := "SELECT balance FROM wallet WHERE serialnum = $1 LIMIT 1"
	row := db.QueryRow(query, pfwsp.SerialNum)
	row.Scan(&currBalance)

	// Calculate
	updatedBalance := currBalance - pfwsp.Amount
	if updatedBalance < 10000 {
		tx.Rollback()
		return PayFromWalletStoreResponse{}, errors.New("insufficient balance")
	}

	// Prepared statement
	updateQuery := "UPDATE wallet SET balance = $1 WHERE serialnum = $2"
	stmt, stmtErr := db.PrepareContext(ctx, updateQuery)
	if err != nil {
		tx.Rollback()
		return PayFromWalletStoreResponse{}, stmtErr
	}

	// Execute update
	_, queryErr := stmt.ExecContext(ctx, updatedBalance, pfwsp.SerialNum)
	if queryErr != nil {
		tx.Rollback()
		return PayFromWalletStoreResponse{}, stmtErr
	}

	err = tx.Commit()
	if err != nil {
		return PayFromWalletStoreResponse{}, stmtErr
	}

	return PayFromWalletStoreResponse{SerialNum: pfwsp.SerialNum, Balance: updatedBalance}, nil
}

func GetIdentityIDBySerialNum(sp GetIdentityIDBySerialNumStoreParam) (IdentityIDWalletStoreResponse, error) {
	db := DBConn()
	defer db.Close()

	var identityID int64
	query := "SELECT identity_id FROM wallet WHERE serialnum = $1 LIMIT 1"
	row := db.QueryRow(query, sp.SerialNum)
	row.Scan(&identityID)

	return IdentityIDWalletStoreResponse{IdentityID: identityID}, nil
}
