package pgstore

import (
	"database/sql"
	"fmt"
	"log"
	"strconv"

	"github.com/spf13/viper"
)

func DBConn() *sql.DB {
	viper.SetConfigFile(".env")
	if err := viper.ReadInConfig(); err != nil {
		fmt.Printf("Error reading config file, %s", err)
	}

	pgHostKey := "WALLET_PG_DB_HOST"
	pgHostPort := "WALLET_PG_DB_PORT"
	pgDbNameKey := "WALLET_PG_DB_NAME"
	pgDbUserKey := "WALLET_PG_DB_USER"
	pgDbPassKey := "WALLET_PG_DB_PASS"

	viper.SetDefault(pgHostKey, "localhost")
	viper.SetDefault(pgHostPort, "5432")
	viper.SetDefault(pgDbNameKey, "wallet")
	viper.SetDefault(pgDbUserKey, "postgres")
	viper.SetDefault(pgDbPassKey, "password")

	pgHost, ok := viper.Get(pgHostKey).(string)
	if !ok {
		log.Panicf("Invalid type assertion for postgresql host")
	}

	pgPort, ok := viper.Get(pgHostPort).(string)
	if !ok {
		log.Panicf("Invalid type assertion for postgresql port")
	}

	pgDbName, ok := viper.Get(pgDbNameKey).(string)
	if !ok {
		log.Panicf("Invalid type assertion for postgresql db name")
	}

	pgUser, ok := viper.Get(pgDbUserKey).(string)
	if !ok {
		log.Panicf("Invalid type assertion for postgresql user")
	}

	pgPass, ok := viper.Get(pgDbPassKey).(string)
	if !ok {
		log.Panicf("Invalid type assertion for postgresql password")
	}

	port, err := strconv.Atoi(pgPort)
	if err != nil {
		log.Panicf("cannot obtain port, %s", err)
	}

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		pgHost, port, pgUser, pgPass, pgDbName)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		log.Panicf("Cannot connect to postgre, %s", err)
	}

	return db
}
