package pgstore

import (
	"log"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq"
)

func Up() {
	db := DBConn()

	driver, err := postgres.WithInstance(db, &postgres.Config{})

	if err != nil {
		log.Panicf("Cannot set driver, %s", err)
	}

	m, err := migrate.NewWithDatabaseInstance(
		"file://wallet/migrations",
		"postgres", driver)
	if err != nil {
		log.Panicf("Cannot set migration, %s", err)
	}

	m.Up()
}
