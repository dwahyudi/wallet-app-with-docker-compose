package repo

import v1 "mywallet/api/v1"

type IWallet interface {
	Create(v1.WalletCreateParam) v1.WalletResponse
	Topup(v1.WalletCreateParam) v1.WalletResponse
	Pay(v1.WalletCreateParam) v1.WalletResponse
}
