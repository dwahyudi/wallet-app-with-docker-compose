package main

import (
	"context"
	"fmt"
	"log"
	v1 "mywallet/api/v1"
	"mywallet/wallet/pgstore"
	"net"

	_ "github.com/lib/pq"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
)

func main() {
	pgstore.Up()

	viper.SetConfigFile(".env")
	if err := viper.ReadInConfig(); err != nil {
		fmt.Printf("Error reading config file, %s", err)
	}
	port := viper.Get("WALLET_GRPC_PORT").(string)

	log.Println("Starting mywallet Wallet GRPC Service")
	log.Println("Listening on", port)

	lis, err := net.Listen("tcp", fmt.Sprintf(":%v", port))
	if err != nil {
		log.Panicf("failed to listen: %v", err)
	}

	ws := WalletServer{}
	grpcServer := grpc.NewServer()

	v1.RegisterWalletsServer(grpcServer, &ws)

	if err := grpcServer.Serve(lis); err != nil {
		log.Panicf("failed to serve: %s", err)
	}
}

type WalletServer struct {
	v1.UnimplementedWalletsServer
}

func (ws *WalletServer) Create(ctx context.Context, b *v1.WalletCreateParam) (*v1.WalletResponse, error) {
	qp := pgstore.CreateWalletStoreParam{IdentityID: b.GetIdentityID(), Balance: b.GetStartingBalance()}
	resp, err := pgstore.CreateWallet(qp)
	if err != nil {
		return &v1.WalletResponse{}, err
	}

	return &v1.WalletResponse{SerialNum: resp.SerialNum, Balance: resp.Balance}, nil
}

func (ws *WalletServer) Topup(ctx context.Context, b *v1.WalletTopupParam) (*v1.WalletResponse, error) {
	qp := pgstore.TopupWalletStoreParam{SerialNum: b.GetSerialNum(), Amount: b.GetAmount()}
	resp, err := pgstore.TopupWallet(qp)
	if err != nil {
		return &v1.WalletResponse{}, err
	}

	return &v1.WalletResponse{SerialNum: resp.SerialNum, Balance: resp.Balance}, nil
}

func (ws *WalletServer) Pay(ctx context.Context, b *v1.WalletPayParam) (*v1.WalletResponse, error) {
	qp := pgstore.PayFromWalletStoreParam{SerialNum: b.GetSerialNum(), Amount: b.GetAmount()}
	resp, err := pgstore.PayFromWallet(qp)
	if err != nil {
		return &v1.WalletResponse{}, err
	}

	return &v1.WalletResponse{SerialNum: resp.SerialNum, Balance: resp.Balance}, nil
}

func (ws *WalletServer) CheckBalance(ctx context.Context, b *v1.WalletCheckBalanceParam) (*v1.WalletResponse, error) {
	qp := pgstore.CheckBalanceWalletStoreParam{SerialNum: b.GetSerialNum()}
	resp, err := pgstore.CheckBalanceWallet(qp)
	if err != nil {
		return &v1.WalletResponse{}, err
	}

	return &v1.WalletResponse{SerialNum: resp.SerialNum, Balance: resp.Balance}, nil
}

func (ws *WalletServer) GetIdentityIDBySerialNum(ctx context.Context, b *v1.WalletSerialNum) (*v1.WalletIdentityID, error) {
	sp := pgstore.GetIdentityIDBySerialNumStoreParam{SerialNum: b.SerialNum}
	resp, err := pgstore.GetIdentityIDBySerialNum(sp)
	if err != nil {
		return &v1.WalletIdentityID{}, err
	}

	return &v1.WalletIdentityID{IdentityID: resp.IdentityID}, nil
}
