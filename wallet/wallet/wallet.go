package wallet

// Wallet represent a physical wallet, it has serial number.
// Each wallet belongs to a provider.
type Wallet struct {
	serialNum  string
	identityID string
	balance    float64
}
