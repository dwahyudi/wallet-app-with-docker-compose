CREATE TABLE public.wallet (
	id serial PRIMARY KEY,
	serialnum VARCHAR ( 50 ) UNIQUE NOT NULL,
	balance DOUBLE PRECISION NOT NULL,
    identity_id INTEGER NOT NULL,
	created_at TIMESTAMP NOT NULL DEFAULT current_timestamp
);

CREATE INDEX wl_serialnum ON wallet (serialnum);
CREATE INDEX wl_identity_id ON wallet (identity_id);
