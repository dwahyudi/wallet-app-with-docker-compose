# Build
FROM golang:1.15.6 AS build-env
ENV APP_PATH="/app"
WORKDIR ${APP_PATH}
COPY go.mod ${APP_PATH}
COPY go.sum ${APP_PATH}
RUN go mod download -x
COPY . ${APP_PATH}
RUN CGO_ENABLED=0 GOOS=linux go build apiservice/cmd/main.go

# Run
FROM alpine:3.12
ENV APP_PATH="/app"
WORKDIR ${APP_PATH}
COPY --from=build-env ${APP_PATH} .
CMD ["./main"]