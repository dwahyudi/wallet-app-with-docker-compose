# Overview

A simple wallet application. This repo has 3 services:
* `wallet` is a service for managing wallet, contains balance for each, handles topup, payment and checking balance.
* `identity` is a service for managing identity, contains database to store users' identity.
* `apiservice` is a RESTful JSON service, it communicates via gRPC with `wallet` (for doing transactions) and `identity` (for checking identity).

# How to run

* Install docker-compose.
* Run `$ cp dev.env .env`
* Run `docker compose up --build` to run the application.

# Operations

## Create A Wallet and An Identity

```bash
curl --location --request POST 'localhost:7777/wallet/create' \
--header 'Content-Type: application/json' \
--data-raw '{
    "starting_balance": 100000,
    "ktp": "787878",
    "name": "banyu",
    "email": "banyu13@gmail.com",
    "phone_number": "098989898989"
}'
```

If successful, above request will result code 201:
```
"Successful wallet creation, serial number:wmfKCL6zC0FdGFw3"
```

Use this serial number to do transactions (topup and pay).

## Topup

```bash
curl --location --request POST 'localhost:7777/wallet/topup' \
--header 'Content-Type: application/json' \
--data-raw '{
    "serial_number": "wmfKCL6zC0FdGFw3",
    "amount": 210000
}'
```

This will add an amount of money to wallet with that serial number.

If fails (for example: identity not valid), it will return 422 code:

`"user identity is not active"`

If successful, it will return 201 code:

`"Successful topup"`

## Pay

```bash
curl --location --request POST 'localhost:7777/wallet/pay' \
--header 'Content-Type: application/json' \
--data-raw '{
    "serial_number": "wmfKCL6zC0FdGFw3",
    "amount": 14333
}'
```

This will add an amount of money to wallet with that serial number.

If fails (for example: identity not valid), it will return 422 code:

`"user identity is not active"`

If successful, it will return 201 code:

`"Successful payment"`

## Check Balance

```bash
curl --location --request GET 'localhost:7777/wallet/checkbalance' \
--header 'Content-Type: application/json' \
--data-raw '{
    "serial_number": "wmfKCL6zC0FdGFw3"
}'
```

This will add an amount of money to wallet with that serial number.

If fails (for example: identity not valid), it will return 422 code:

`"user identity is not active"`

If balance is insufficient, it will also return 422 code:

`"insufficient balance"`

If successful, it will return 200 code:

`"34554"`